#include <iostream>
#include <string>
#include <iomanip>
#include "boxes.h"

using namespace std;
byte X[8];
byte Y[8];

int main (int argc, char *argv[])
{
	//Begin Generating Stage Keys
	byte R[9];
	byte K[19][8];
	byte KM[8] = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08};


	/*unsigned int KM[8];
           unsigned char str_len = strlen(argv[1]);
    
            for (int i = 0; i < (str_len / 2); i++) {
            sscanf(argv[1] + 2*i, "%02x", &KM[i]);
            printf("KM %d: %02x\n", i, KM[i]);
           }*/


	for(int i = 0; i <= 7; i++)
	{
		R[i] = KM[i];
		R[8] = R[0] ^ R[1] ^ R[2] ^ R[3] ^ R[4] ^ R[5] ^ R[6] ^ R[7];
		K[0][i] = R[i];
	}

	for(int i = 0; i <= 18; i++)
	{
		for( int j = 0; j <= 8; j++)
		{
			R[j] = (R[j] << 3)|(R[j] >> 5);
		}

		for( int k = 0; k <=7; k++)
		{
			K[i][k] = (R[(i+k)%9] + B[i][k]);
		}
		
		
	}
	//END Stage Key Generation
  
	//Get Message
	string m;
	cout << "Enter Message: ";
	getline(cin,m);
	m.resize(120, NULL);

	for(int c = 0; c<15; c++)
	{
		for(int i=0; i<8; i++)
		{
			X[i] = m[i];
		}
		m = m.erase(0,8);
		
	//END Get Message

	//Rounds 1-8
	if(X[0] != NULL)
	{
	for(int r = 0; r <=7; r++)
	{
		//A
		for(int j = 0; j <= 7; j++)
		{
			switch (j)
			{
			case 0: 
				X[j] = X[j] ^ K[2*r][j];
				break;
			case 1:
				X[j] = (X[j] + K[2*r][j]);
				break;
			case 2:
				X[j] = (X[j] + K[2*r][j]);
				break;
			case 3: 
				X[j] = X[j] ^ K[2*r][j];
				break;
			case 4: 
				X[j] = X[j] ^ K[2*r][j];
				break;
			case 5:
				X[j] = (X[j] + K[2*r][j]);
				break;
			case 6:
				X[j] = (X[j] + K[2*r][j]);
				break;
			case 7: 
				X[j] = X[j] ^ K[2*r][j];
				break;
			}
		}

		//B
		for(int j = 0; j <= 7; j++)
		{
			switch (j)
			{
			case 0: 
				X[j] = S[X[j]];
				break;
			case 1:
				X[j] = SI[X[j]];
				break;
			case 2:
				X[j] = SI[X[j]];
				break;
			case 3: 
				X[j] = S[X[j]];
				break;
			case 4: 
				X[j] = S[X[j]];
				break;
			case 5:
				X[j] = SI[X[j]];
				break;
			case 6:
				X[j] = SI[X[j]];
				break;
			case 7: 
				X[j] = S[X[j]];
				break;
			}
		}

		//C
		for(int j = 0; j <= 7; j++)
		{
			switch (j)
			{
			case 0: 
				X[j] = X[j] + K[2*r+1][j];
				break;
			case 1:
				X[j] = X[j] ^ K[2*r+1][j];
				break;
			case 2:
				X[j] = X[j] ^ K[2*r+1][j];
				break;
			case 3: 
				X[j] = X[j] + K[2*r+1][j];
				break;
			case 4: 
				X[j] = X[j] + K[2*r+1][j];
				break;
			case 5:
				X[j] = X[j] ^ K[2*r+1][j];
				break;
			case 6:
				X[j] = X[j] ^ K[2*r+1][j];
				break;
			case 7: 
				X[j] = X[j] + K[2*r+1][j];
				break;
			}
		}

		//D
		for(int j = 0; j <= 6; j++)
		{
			switch (j)
			{
			case 0: 
				X[j] = (2*X[j])+X[j+1];
				X[j+1] = X[j]+X[j+1];
				break;
			case 2:
				X[j] = (2*X[j])+X[j+1];
				X[j+1] = X[j]+X[j+1];
				break;
			case 4: 
				X[j] = (2*X[j])+X[j+1];
				X[j+1] = X[j]+X[j+1];
				break;
			case 6:
				X[j] = (2*X[j])+X[j+1];
				X[j+1] = X[j]+X[j+1];
				break;
			}
		}




		//E
		Y[0] = (2*X[0])+X[2];
		Y[1] = X[0]+X[2];

		Y[2] = (2*X[4])+X[6];
		Y[3] = X[4]+X[6];

		Y[4] = (2*X[1])+X[3];
		Y[5] = X[1]+X[3];

		Y[6] = (2*X[5])+X[7];
		Y[7] = X[5]+X[7];
		for (int j = 0; j < 8; j++)
		{
			X[j] = Y[j];
		}

		//F
		Y[0] = (2*X[0])+X[2];
		Y[1] = X[0]+X[2];

		Y[2] = (2*X[4])+X[6];
		Y[3] = X[4]+X[6];

		Y[4] = (2*X[1])+X[3];
		Y[5] = X[1]+X[3];

		Y[6] = (2*X[5])+X[7];
		Y[7] = X[5]+X[7];
		for (int j = 0; j < 8; j++)
		{
			X[j] = Y[j];
		}
	}
	
	//Round 9
	for(int l = 0; l <= 7; l++)
		{
			switch (l)
			{
			case 0: 
				X[l] = X[l] + K[18][l];
				break;
			case 1:
				X[l] = X[l] ^ K[18][l];
				break;
			case 2:
				X[l] = X[l] ^ K[18][l];
				break;
			case 3: 
				X[l] = X[l] + K[18][l];
				break;
			case 4: 
				X[l] = X[l] + K[18][l];
				break;
			case 5:
				X[l] = X[l] ^ K[18][l];
				break;
			case 6:
				X[l] = X[l] ^ K[18][l];
				break;
			case 7: 
				X[l] = X[l] + K[18][l];
				break;
			}
	}
	//END Round 9
	
	for(int k = 0; k<8; k++)
	{
		printf("%x",X[k]);
	}
	cout << endl;
	}
	}

	char Enter;
	cin.ignore();
	cin.get(Enter);
	
	return 0;
}